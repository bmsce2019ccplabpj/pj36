#include <stdio.h>
int input()
{
    int n;
    printf("enter a number:\n");
    scanf("%d",&n);
    return n;
}
void output(int a,int b)
{
    printf("the numbers before swapping:\na=%d\nb=%d",a,b);
}
void swap(int *a,int *b)
{
    int t;
    t=*a;
    *a=*b;
    *b=t;
}
void output1(int a,int b)
{
    printf("\nthe numbers after swapping:\na=%d\nb=%d",a,b);
}
int main()
{
    int a,b;
    a=input();
    b=input();
    output(a,b);
    swap(&a,&b);
    output1(a,b);
    return 0;
}