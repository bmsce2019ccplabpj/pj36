#include <stdio.h>
#include <math.h>
float input()
{
    float a;
    printf("enter the point\n");
    scanf("%f",&a);
    return a;
}
float compute(float x1,float y1,float x2,float y2)
{
    float d;
    d=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    return d;
}
void output(float d)
{
    printf("the distance %f",d);
    return;
}
int main()
{
    float x1,y1,x2,y2,d;
    x1=input();
    y1=input();
    x2=input();
    y2=input();
    d=compute(x1,y1,x2,y2);
    output(d);
    return 0;
}