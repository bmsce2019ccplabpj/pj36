#include <stdio.h>
int main()
{
	int num,i,n,pos=-1,beg,end,mid,found=0;
	printf("enter the no.of elements in the array: ");
	scanf("%d",&n);
	int a[n];
	printf("enter the elements\n");
	for(i=0;i<n;i++)
	{
		scanf("%d",&a[i]);
	}
	printf("enter the no. that has to be searched: ");
	scanf("%d",&num);
	beg=0,end=n-1;
	while(beg<=end)
	{
		mid=(beg+end)/2;
		if(a[mid]==num)
		{
			printf("%d is present in the array at the position =%d",num,mid);
			found=1;
			break;
		}
		else if(a[mid]>num)
		end=mid-1;
		else
		beg=mid+1;
	}
	if(beg>end&&found==0)
	printf("%d doesn't exist in the array",num);
	return 0;
}