#include <stdio.h>
struct date
{
    int day;
    int month;
    int year;
};
struct employee
{
    int ID;
    char name[20];
    double salary;
    struct date DOJ;
};
typedef struct employee Employee;
int main()
{
    Employee E;
    printf("enter the employee details");
    printf("\nEmployee ID:");
    scanf("%d",&E.ID);
    printf("\nEmployee name:");
    scanf("%s",E.name);
    printf("\nEmployee salary:");
    scanf("%lf",&E.salary);
    printf("\nEmployee DOJ:");
    scanf("%d%d%d",&E.DOJ.day,&E.DOJ.month,&E.DOJ.year);
    printf("\nEmployee ID:%d",E.ID);
    printf("\nEmployee name:%s",E.name);
    printf("\nEmployee salary:%lf",E.salary);
    printf("\nEmployee DOJ:%d/%d/%d",E.DOJ.day,E.DOJ.month,E.DOJ.year);
    return 0;
}