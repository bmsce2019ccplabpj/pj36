#include <stdio.h>
struct fraction
{
	int num;
	int denom;
};
typedef struct fraction Fraction;
int size()
{
	int n;
	printf("enter the size of the array\n");
	scanf("%d",&n);
	return n;
}
void array(int n, Fraction f[n])
{
	int i;
	for(i=0;i<n;i++)
	{
		printf("enter the fraction\n");
		scanf("%d%d",&f[i].num,&f[i].denom);
	}
}
Fraction add(int n, Fraction a[n])
{
	Fraction sum;
	sum.num=0;
	sum.denom=0;
	for(int i=0;i<n;i++)
	{
		sum.num=(a[i].num*a[i++].denom)+(a[i].denom*a[i++].num);
		sum.denom=a[i].denom*a[i++].denom;
	}
}
int gcd(int n, Fraction a[n])
{
	int result;
	for(int i=0;i<n;i++)
	{
		result=a[i].denom%a[i+1].denom;
		if(a[i+1].denom!=0)
			return gcd(a[i+1].denom,a[i].denom%a[i+1].denom);
		else
			return a[i].denom;
	}
}
Fraction compute(int n,Fraction f[n],int gcd)
{
	Fraction sum;
	sum.num=sum.num/gcd;
	sum.denom=sum.denom/gcd;
	return sum;
}
void output(Fraction sum)
{
	printf("sum=%d/%d",sum.num,sum.denom);
}
int main()
{
	int n,g;
	n=size();
	Fraction f[n],s;
	array(n,f);
	g=gcd(n,f);
	s=compute(n,f,g);
	output(s);
}