#include <stdio.h>
int input()
{
    int n;
    printf("enter a number:\n");
    scanf("%d",&n);
    return n;
}
void swap(int *a,int *b)
{
    int t;
    t=*a;
    *a=*b;
    *b=t;
}
void output(int a,int b)
{
    printf("the numbers after swapping:\na=%d,\nb=%d",a,b);
}
int main()
{
    int a,b;
    a=input();
    b=input();
    swap(&a,&b);
    output(a,b);
    return 0;
}