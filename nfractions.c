#include <stdio.h>
struct fraction{
	int num;
	int denom;
};
typedef struct fraction Fraction;
int size()
{
	int n;
	printf("enter the size of the array\n");
	scanf("%d",&n);
	return n;
}
void array(int n, Fraction f[n])
{
	int i;
	for(i=0;i<n;i++)
	{
		printf("enter the fractions\n");
		scanf("%d/%d",&f[i].num,&f[i].denom);
	}
}
Fraction add(int n,Fraction f[n])
{
	int i,x;
	Fraction sum;
	sum.num=f[0].num;
	sum.denom=f[0].denom;
	for(i=1;i<n;i++)
	{
		sum.num=(sum.num*f[i].denom)+(sum.denom*f[i].num);
		sum.denom=sum.denom*f[i].denom;
	}
	for(int i=0;i<n;i++)
	{
		x=gcd(sum.denom,sum.num);
	}
	sum.num=sum.num/x;
	sum.denom=sum.denom/x;
	return sum;
}
int gcd(int a,int b)
{
	int g;
	for(int i=1;i<a&&i<b;i++)
	{
		if(a%i==0&&b%i==0)
			g=i;
	}
	return g;
}
void output(int n,Fraction f[n],Fraction sum)
{
	for(int i=0;i<n;i++)
	{
	 if(i==(n-1))
	 {
		printf("%d/%d ",f[i].num,f[i].denom);
	 }
	 else
	 {
		printf("%d/%d + ",f[i].num,f[i].denom);
	 }
	}
	printf("= %d/%d",sum.num,sum.denom);
}
int main()
{
	int n,x;
	n=size();
	Fraction f[n],s;
	array(n,f);
	s=add(n,f);
	output(n,f,s);
	return 0;
}