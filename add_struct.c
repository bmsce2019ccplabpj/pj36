#include <stdio.h>
struct fraction
{
    int num;
    int denom;
};
typedef struct fraction Fraction;
Fraction input()
{
    Fraction f;
    printf("enter the fraction\n");
    scanf("%d/%d",&f.num,&f.denom);
    return f;
}
Fraction add(Fraction f1,Fraction f2)
{
    Fraction sum;
    sum.num=f1.num*f2.denom+f2.num*f1.denom;
    sum.denom=f1.denom*f2.denom;
    return sum;
}
void output(Fraction f1,Fraction f2,Fraction sum)
{
    printf("the sum of %d/%d and %d/%d is = %d/%d",f1.num,f1.denom,f2.num,f2.denom,sum.num,sum.denom);
    return;
}
int main()
{
    Fraction f1,f2,sum;
    f1=input();
    f2=input();
    sum=add(f1,f2);
    output(f1,f2,sum);
    return 0;
}