#include <stdio.h>
#include <math.h>
int main()
{
    int a,b,c,d;
    float r1,r2;
    printf("enter any three integers\n");
    scanf("%d%d%d",&a,&b,&c);
    d=sqrt(b*b-4*a*c);
    r1=(-b+d)/2*a;
    r2=(-b-d)/2*a;
    if(d>=0)
    {
        printf("the roots %f,%f are real and distinct",r1,r2);
    }
    else if(d==0)
    {
        printf("the roots %f,%f are real and equal",r1,r2);
    }
    else
    {
        printf("the roots r1,r2 are complex roots");
    }
    return 0;
}