#include <stdio.h>
#include <math.h>
struct point
{
    float x;
    float y;
};
typedef struct point Point;
Point input()
{
    Point p;
    printf("enter the coordinates of the point\n");
    scanf("%f%f",&p.x,&p.y);
    return p;
}
float distance(Point p1,Point p2)
{
    float d;
    d=sqrt((p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y));
    return d;
}
void output(float d)
{
    printf("the distance is = %f",d);
    return;
}
int main()
{
    Point p1,p2;
    float d;
    p1=input();
    p2=input();
    d=distance(p1,p2);
    output(d);
    return 0;
}