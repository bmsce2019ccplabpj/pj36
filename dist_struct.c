#include <stdio.h>
#include <math.h>
struct point
{
		float x;
		float y;
};
typedef struct point Point;
Point input_point()
{
		Point P;
		printf("enter x,y coordinates of the point\n");
		scanf("%f%f",&P.x,&P.y);
		return P;
}
float compute(Point P1,Point P2)
{
		float d;
		d=sqrt((P2.x-P1.x)*(P2.x-P1.x)+(P2.y-P1.y)*(P2.y-P1.y));
		return d;
}
void output(float d)
{
		printf("the distance betweenthe points is = %f",d);
}
int main()
{
		Point P1,P2;
		float d;
		P1=input_point();
		P2=input_point();
		d=compute(P1,P2);
		output(d);
		return 0;
}


#include <stdio.h>
#include <math.h>
struct point
{
		float x;
		float y;
};
typedef struct point Point;
Point input()
{
		Point p;
		printf("enter the coordinates of the point\n");
		scanf("%f%f",&p.x,&p.y);
		return p;
}
float  find_distance(Point p1,Point p2)
{
		float distance;
		distance=sqrt((p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y));
		return distance;
}
void output(Point p1,Point p2,float distance)
{
		printf("the distance between %f,%f and %f,%f is %f",p1.x,p1.y,p2.x,p2.y,distance);
		return;
}
int main()
{
	Point p1,p2;
	float distance;
	p1=input();
	p2=input();
	distance=find_distance(p1,p2);
	output(p1,p2,distance);
	return 0;
}