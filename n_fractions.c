#include <stdio.h>
struct fraction{
	int n;
	int d;
};
typedef struct fraction Fraction;
int size()
{
	int n;
	printf("enter the size of the array\n");
	scanf("%d",&n);
	return n;
}
void array(int n, Fraction f[n])
{
	int i;
	for(i=0;i<n;i++)
	{
		printf("enter the fractions\n");
		scanf("%d/%d",&f[i].n,&f[i].d);
	}
}
Fraction add(int n,Fraction f[n])
{
	int i;
	Fraction sum;
	sum.n=f[0].n;
	sum.d=f[0].d;
	for(i=1;i<n;i++)
	{
		sum.n=(sum.n*f[i].d)+(sum.d*f[i].n);
		sum.d=sum.d*f[i].d;
	}
	return sum;
}
int gcd(int a,int b)
{
	int g;
	for(int i=1;i<a&&i<b;i++)
	{
		if(a%i==0&&b%i==0)
			g=i;
	}
	return g;
}
int gcd_s(int n,Fraction f[n])
{
	int g;
	for(int i=0;i<n;i++)
	{
		g=gcd(f[i].d,f[i+1].d);
	}
	return g;
}
Fraction simplify(Fraction sum,int x)
{
	sum.n=sum.n/x;
	sum.d=sum.d/x;
	return sum;
}
void output(Fraction sum)
{
	printf("sum=%d/%d",sum.n,sum.d);
}
int main()
{
	int n,x;
	n=size();
	Fraction f[n],s,h;
	array(n,f);
	s=add(n,f);
	x=gcd_s(n,f);
	h=simplify(s,x);
	output(h);
	return 0;
}




#include <stdio.h>
struct fraction{
	int n;
	int d;
};
typedef struct fraction Fraction;
int size()
{
	int n;
	printf("enter the size of the array\n");
	scanf("%d",&n);
	return n;
}
void array(int n, Fraction f[n])
{
	int i;
	for(i=0;i<n;i++)
	{
		printf("enter the fractions\n");
		scanf("%d/%d",&f[i].n,&f[i].d);
	}
}
Fraction add(int n,Fraction f[n])
{
	int i;
	Fraction sum;
	sum.n=f[0].n;
	sum.d=f[0].d;
	for(i=1;i<n;i++)
	{
		sum.n=(sum.n*f[i].d)+(sum.d*f[i].n);
		sum.d=sum.d*f[i].d;
	}
	return sum;
}
int gcd(int a,int b)
{
	int g;
	for(int i=1;i<a&&i<b;i++)
	{
		if(a%i==0&&b%i==0)
			g=i;
	}
	return g;
}
int gcd_s(int n,Fraction sum)
{
	int g;
	for(int i=0;i<n;i++)
	{
		g=gcd(sum.d,sum.n);
	}
	return g;
}
Fraction simplify(Fraction sum,int x)
{
	sum.n=sum.n/x;
	sum.d=sum.d/x;
	return sum;
}
void output(Fraction sum)
{
	printf("sum=%d/%d",sum.n,sum.d);
}
int main()
{
	int n,x;
	n=size();
	Fraction f[n],s,h;
	array(n,f);
	s=add(n,f);
	x=gcd_s(n,s);
	h=simplify(s,x);
	output(h);
	return 0;
}



#include <stdio.h>
struct fraction{
	int n;
	int d;
};
typedef struct fraction Fraction;
int size()
{
	int n;
	printf("enter the size of the array\n");
	scanf("%d",&n);
	return n;
}
void array(int n, Fraction f[n])
{
	int i;
	for(i=0;i<n;i++)
	{
		printf("enter the fractions\n");
		scanf("%d/%d",&f[i].n,&f[i].d);
	}
}
Fraction add(int n,Fraction f[n])
{
	int i,x;
	Fraction sum;
	sum.n=f[0].n;
	sum.d=f[0].d;
	for(i=1;i<n;i++)
	{
		sum.n=(sum.n*f[i].d)+(sum.d*f[i].n);
		sum.d=sum.d*f[i].d;
	}
	for(int i=0;i<n;i++)
	{
		x=gcd(sum.d,sum.n);
	}
	sum.n=sum.n/x;
	sum.d=sum.d/x;
	return sum;
}
int gcd(int a,int b)
{
	int g;
	for(int i=1;i<a&&i<b;i++)
	{
		if(a%i==0&&b%i==0)
			g=i;
	}
	return g;
}
void output(Fraction sum)
{
	printf("sum=%d/%d",sum.n,sum.d);
}
int main()
{
	int n,x;
	n=size();
	Fraction f[n],s;
	array(n,f);
	s=add(n,f);
	output(s);
	return 0;
}





#include <stdio.h>
struct fraction{
	int num;
	int denom;
};
typedef struct fraction Fraction;
int size()
{
	int n;
	printf("enter the size of the array\n");
	scanf("%d",&n);
	return n;
}
void array(int n, Fraction f[n])
{
	int i;
	for(i=0;i<n;i++)
	{
		printf("enter the fractions\n");
		scanf("%d/%d",&f[i].num,&f[i].denom);
	}
}
Fraction add(int n,Fraction f[n])
{
	int i,x;
	Fraction sum;
	sum.num=f[0].num;
	sum.denom=f[0].denom;
	for(i=1;i<n;i++)
	{
		sum.num=(sum.num*f[i].denom)+(sum.denom*f[i].num);
		sum.denom=sum.denom*f[i].denom;
	}
	for(int i=0;i<n;i++)
	{
		x=gcd(sum.denom,sum.num);
	}
	sum.num=sum.num/x;
	sum.denom=sum.denom/x;
	return sum;
}
int gcd(int a,int b)
{
	int g;
	for(int i=1;i<a&&i<b;i++)
	{
		if(a%i==0&&b%i==0)
			g=i;
	}
	return g;
}
void output(Fraction sum)
{
		printf("%d/%d",sum.num,sum.denom);
}
int main()
{
	int n,x;
	n=size();
	Fraction f[n],s;
	array(n,f);
	s=add(n,f);
	output(s);
	return 0;
}